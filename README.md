# Bot Builder
Targeted at creating vehicle models described using SDFormat(.sdf) version 1.9 for use with Ignition Fortress

## TODO
Update units conversion so that float values are given as 
```yaml
wheelbase: [1.32123, 'in']
```

Update the target for this:
* Gazebo Harmonic? (SDF)
* Open3D Engine? (SDF)

Add more sensors to the program.

## Support Gazebo Versions
* TBD

## Support
* Model
* Links
* Geometry (rectangle, cylinder)
* Visuals (basic geometry)
* Collision (basic geometry)

## Supported Sensors
None

## Supported Plugins
* Ignition Ackermann Steering