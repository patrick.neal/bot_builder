from typing import Dict, List
import yaml
import os
from math import pi

from builder import factory
from builder.geometries import Rectangle, Cylinder, Inertial
from builder.joints import Revolute2, Axis, Revolute
from builder.entities import Link, Model, Visual, Collision
from builder.pose import Pose
from builder.ign_plugin import CimarAckermannPlugin

"""
    Plugin should check if all the required parameters are present in the yaml file.

"""

class AckermannBuilder:

    def __init__(self):
        pass

    def input(self):
        filename = input("Input filename for parameters: ")

        # Get the directory of this python project
        dir_path = os.path.dirname(os.path.realpath(__file__))
        dir_path = os.path.dirname(dir_path)

        with open(dir_path + '/robot_parameters/' + filename) as file:
            data = yaml.safe_load(file)
        
        # Need to convert all input units to meters, kilograms-mass
        conversions = {}
        length_conversion = {'in': 0.0254, 'ft': 0.3048, 'cm': 0.01, 'm': 1.0}
        if (data['units']['length'] in length_conversion):
            conversions['length'] = length_conversion[data['units']['length']]
        else:
            print("Units are not valid. Use: 'in', 'ft', 'cm', 'm'")

        mass_conversion = {'lb': 0.453592, 'kg': 1.0}
        if(data['units']['weight'] in mass_conversion):
            conversions['weight'] = mass_conversion[data['units']['weight']]
        else:
            print("Units are not valid. Use: 'lb', 'kg'")
        
        robot_parameters = {}
        if 'name' in data:
            robot_parameters['name'] = data['name']
        else:
            print("Missing name")

        # Need to validate certain options in the yaml file
        def convert_wheel_units(wheel: Dict[str, float], conversions: Dict[str, float]) -> Dict[str, float]:
            converted_wheel = {}
            converted_wheel['weight'] = wheel['weight'] * conversions['weight']
            converted_wheel['diameter'] = wheel['diameter'] * conversions['length']
            converted_wheel['width'] = wheel['width'] * conversions['length']
            return converted_wheel

        if 'wheels' in data['parameters']:
            # Use single wheel parameters for all wheels
            robot_parameters['front_wheels'] = convert_wheel_units(data['parameters']['wheels'], conversions)
            robot_parameters['rear_wheels'] = convert_wheel_units(data['parameters']['wheels'], conversions)
        elif('front_wheels' in data['parameters'] and 'rear_wheels' in data['parameters']):
            robot_parameters['front_wheels'] = convert_wheel_units(data['parameters']['front_wheels'], conversions)
            robot_parameters['rear_wheels'] = convert_wheel_units(data['parameters']['rear_wheels'], conversions)
        else:
            print("Missing required wheel information. Use 'wheels' or 'front_wheels' and 'rear_wheels'")
        
        if 'track' in data['parameters']:
            robot_parameters['front_track'] = data['parameters']['track'] * conversions['length']
            robot_parameters['rear_track'] = data['parameters']['track'] * conversions['length']
        elif( 'front_track' in data['parameters']  and 'rear_track' in data['parameters']):
            robot_parameters['front_track'] = data['parameters']['front_track'] * conversions['length']
            robot_parameters['rear_track'] = data['parameters']['rear_track'] * conversions['length']
        else:
            print("Missing required track information. Use 'track' or 'front_track' and 'rear_track' in yaml file")

        if 'wheelbase' in data['parameters']:
            robot_parameters['wheelbase'] = data['parameters']['wheelbase'] * conversions['length']
        else:
            raise KeyError("Missing required wheelbase information. Use 'wheelbase' in yaml file")

        if 'kingpin_width' in data['parameters']:
            robot_parameters['kingpin_width'] = data['parameters']['kingpin_width'] * conversions['length']

        def convert_chassis_units(chassis: Dict[str, float], conversions: Dict[str, float]) -> Dict[str, float]:
            converted_chassis = {}
            converted_chassis['mass'] = chassis['weight'] * conversions['weight']
            converted_chassis['length'] = chassis['length'] * conversions['length']
            converted_chassis['width'] = chassis['width'] * conversions['length']
            converted_chassis['height'] = chassis['height'] * conversions['length']
            converted_chassis['ground_clearance'] = chassis['ground_clearance'] * conversions['length'] if chassis['ground_clearance'] > 0 else 0
            return converted_chassis

        if 'chassis' in data['parameters']:
            robot_parameters['chassis'] = convert_chassis_units(data['parameters']['chassis'], conversions)
        else:
            print("Missing required chassis information. Use 'chassis' in yaml file")

        if 'plugins' in data:
            robot_parameters['plugins'] = data['plugins']

        return robot_parameters

    def build(self) -> Model:
        """
            Performs all the functions to create the robot .sdf file"
            This Robot contains 6 entities
                - Front Left Kingpin
                - Front Right Kingpin
                - Front Left Wheel
                - Front Right Wheel
                - Rear Left Wheel
                - Rear Right Wheel
                - Chassis

            All poses are with respect to the chassis.
            The model is built "from the ground up" starting with the geometries and building up the links.
            Then all are combined into the model
        """

        input_params = self.input()

        # Robot parameters converted to important dimensions
        x_offset = input_params['wheelbase'] / 2.0
        front_y_offset = input_params['front_track'] / 2.0
        if 'kingpin_width' in input_params:
            kingpin_offset = input_params['kingpin_width'] / 2.0
        else:
            kingpin_offset = front_y_offset
        rear_y_offset = input_params['rear_track'] / 2.0

        # Create all the links of the model
        links = []

        chassis = self.create_chassis(chassis_params=input_params['chassis'])
        links.append(chassis)

        # Where should the wheel centers be to obtain the specified ground clearance?
        fw_z_offset = 0.5 * input_params['chassis']['height'] + (input_params['chassis']['ground_clearance'] - input_params['front_wheels']['diameter'] / 2.0)
        rw_z_offset = 0.5 * input_params['chassis']['height'] + (input_params['chassis']['ground_clearance'] - input_params['rear_wheels']['diameter'] / 2.0)

        left_kingpin_pose = Pose(translation=[x_offset, kingpin_offset, -fw_z_offset], rotation=[0, 0, 0], relative_to='chassis')
        right_kingpin_pose = Pose(translation=[x_offset, -kingpin_offset, -fw_z_offset], rotation=[0, 0, 0], relative_to='chassis')

        fl_wheel_pose = Pose(translation=[x_offset, front_y_offset, -fw_z_offset], rotation=[-90, 0, 0], relative_to='chassis')
        fr_wheel_pose = Pose(translation=[x_offset, -front_y_offset, -fw_z_offset], rotation=[-90, 0, 0], relative_to='chassis')
        rl_wheel_pose = Pose(translation=[-x_offset, rear_y_offset, -rw_z_offset], rotation=[-90, 0, 0], relative_to='chassis')
        rr_wheel_pose = Pose(translation=[-x_offset, -rear_y_offset, -rw_z_offset], rotation=[-90, 0, 0], relative_to='chassis')

        left_kingpin = self.create_kingpin(name='left_kingpin', pose=left_kingpin_pose)
        links.append(left_kingpin)
        right_kingpin = self.create_kingpin(name='right_kingpin', pose=right_kingpin_pose)
        links.append(right_kingpin)

        fl_wheel = self.create_wheel(name='front_left_wheel', wheel_params=input_params['front_wheels'], pose=fl_wheel_pose)
        links.append(fl_wheel)
        fr_wheel = self.create_wheel(name='front_right_wheel', wheel_params=input_params['front_wheels'], pose=fr_wheel_pose)
        links.append(fr_wheel)
        rl_wheel = self.create_wheel(name='rear_left_wheel', wheel_params=input_params['rear_wheels'], pose=rl_wheel_pose)
        links.append(rl_wheel)
        rr_wheel = self.create_wheel(name='rear_right_wheel', wheel_params=input_params['rear_wheels'], pose=rr_wheel_pose)
        links.append(rr_wheel)
        
        # Create all the required joints for the model
        joints = []
        fl_steer_joint = Revolute(name='fl_steer_joint', parent='chassis', child='left_kingpin', axis=Axis(axis_vector=[0, 0, 1]))
        joints.append(fl_steer_joint)
        fr_steer_joint = Revolute(name='fr_steer_joint', parent='chassis', child='right_kingpin', axis=Axis(axis_vector=[0, 0, 1]))
        joints.append(fr_steer_joint)
        fl_wheel_joint = Revolute(name='fl_wheel_joint', parent='left_kingpin', child='front_left_wheel', axis=Axis(axis_vector=[0, 0, 1]))
        joints.append(fl_wheel_joint)
        fr_wheel_joint = Revolute(name='fr_wheel_joint', parent='right_kingpin', child='front_right_wheel', axis=Axis(axis_vector=[0, 0, 1]))
        joints.append(fr_wheel_joint)
        rl_wheel_joint = Revolute(name='rl_wheel_joint', parent='chassis', child='rear_left_wheel', axis=Axis(axis_vector=[0, 0, 1]))
        joints.append(rl_wheel_joint)
        rr_wheel_joint = Revolute(name='rr_wheel_joint', parent='chassis', child='rear_right_wheel', axis=Axis(axis_vector=[0, 0, 1]))
        joints.append(rr_wheel_joint)

        # ASSUMES THERE IS A PLUGIN, dont have knowledge of plugins at this point.
        plugins = []
        if 'plugins' in input_params:
            # Create dictionary for CimarAckermannPlugin
            params = {  'front_left_joint': 'fl_wheel_joint', 'rear_left_joint': 'rl_wheel_joint',
                        'front_right_joint': 'fr_wheel_joint', 'rear_right_joint': 'rr_wheel_joint',
                        'left_steering_joint': 'fl_steer_joint', 'right_steering_joint': 'fr_steer_joint',
                        'kingpin_width': input_params['kingpin_width'], 'wheelbase': input_params['wheelbase'],
                        'track': input_params['front_track'], 'front_wheel_radius': input_params['front_wheels']['diameter']/2,
                        'rear_wheel_radius': input_params['rear_wheels']['diameter']/2 
            }
            params.update(input_params['plugins']['cimar_ackermann'])
            
            if 'steering_limit' in params:
                params['steering_limit'] = params['steering_limit']*pi/180

            plugins.append(CimarAckermannPlugin(params))

        ackermann_model = Model(name=input_params['name'], canonical_link='chassis', links=links, joints=joints, plugins=plugins)

        return [ackermann_model]
    
    def create_chassis(self, chassis_params: Dict[str, float]) -> Link:
        """ Creates a "chassis" link that represents the chassis of the robot"""
        z_offset = 0.5 * chassis_params['height'] + chassis_params['ground_clearance']
        pose = Pose(translation=[0, 0, z_offset], rotation=[0, 0, 0], relative_to='__model__')
        body_mass =  chassis_params['mass']
        body = Rectangle(length=chassis_params['length'], width=chassis_params['width'], height=chassis_params['height'])
        visual = Visual(name='visual', geometry=body)
        collision = Collision(name='collision', geometry=body)
        inertial = Inertial(geometry=body, mass=body_mass)

        return Link(name='chassis', inertial=inertial, collision=collision, visuals=[visual], pose=pose)
    
    def create_wheel(self, name: str, wheel_params: Dict[str, float], pose: Pose) -> Link:
        """ Creates a "wheel" link using default visual parameters"""
        wheel_radius = wheel_params['diameter'] / 2.0
        wheel_width = wheel_params['width']
        wheel_mass = wheel_params['weight']
        
        wheel_geom = Cylinder(radius=wheel_radius, width=wheel_width)
        visual = Visual(name='visual', geometry=wheel_geom)
        collision = Collision(name='collision', geometry=wheel_geom)
        inertial = Inertial(geometry=wheel_geom, mass=wheel_mass)

        return Link(name=name, inertial=inertial, collision=collision, visuals=[visual], pose=pose)

    def create_kingpin(self, name: str, pose: Pose) -> Link:
        kingpin_geom = Cylinder(radius=0.0254, width=0.0762)
        visual = Visual(name='visual', geometry=kingpin_geom)

        return Link(name=name, inertial=None, collision=None, visuals=[visual], pose=pose)


def initialize():
    print('Ackermann Steer plugin has been loaded.')
    factory.register('Ackermann Steered', AckermannBuilder)
