from builder import factory

class DiffDriveBuilder:
    
    def build(self) -> None:
        print("You have selected a differential driver robot")


def initialize() -> None:
    print("Diff Drive plugin loaded.")
    factory.register('Diff. Drive', DiffDriveBuilder)