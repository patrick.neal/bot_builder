import yaml
import copy
import os

from builder import loader
from builder import factory
from builder import sdf_writer
import builder.geometries as gm
import builder.entities as ent
import builder.joints as jt

import xml.etree.ElementTree as ET

def main() -> None:

    print("Welcome to Bot Builder")

    path = os.path.dirname(os.path.realpath(__file__))

    # Load Plugins
    with open(path + '/bots.yaml') as file:
        data = yaml.safe_load(file)
        loader.load_plugins(data['plugins'])

    print("What kind of Bot do you wish to build?")

    option_chars = ['A','B','C','D','E','F','G','H','I','J','K','L',
                    'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

    alphabet_count = 0
    options_dict = {}
    for key, value in factory.robot_creation_funcs.items():
        option = option_chars[alphabet_count] + ": " + str(key)
        options_dict[option_chars[alphabet_count]] = key
        print(option)
        alphabet_count += 1

    # need to create a mapping between inputs (Aa-Zz) and bot names
    choice = input("Choose a robot type: ")
    choice = choice.capitalize()

    robot_type = options_dict.get(choice)

    if robot_type is not None:
        builder = factory.create(robot_type)
        robot_models = builder.build()
        for robot_model in robot_models:
            sdf_writer.write_model(robot_model)
    else:
        print("Invalid option selected.")


if __name__ == '__main__':
    main()