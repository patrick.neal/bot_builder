from typing import Dict

compatible_units = ['in', 'ft', 'cm', 'm', 'lb', 'kg']

#length_conversion = {'in': 0.0254, 'ft': 0.3048, 'cm': 0.01, 'm': 1.0}

#mass_conversion = {'lb': 0.453592, 'kg': 1.0}

conversions = {'in': 0.0254, 'ft': 0.3048, 'cm': 0.01, 'm': 1.0, 'lb': 0.453592, 'kg': 1.0}

def convert_units(data: Dict)-> Dict:
  
    global_conversions = {} # Stores global conversion factors
    if (data['units']['length'] in conversions):
        global_conversions['length'] = conversions[data['units']['length']]
    else:
        print("Units are not valid. Use: 'in', 'ft', 'cm', 'm'")

    if(data['units']['weight'] in conversions):
        global_conversions['weight'] = conversions[data['units']['weight']]
    else:
        print("Units are not valid. Use: 'lb', 'kg'")

    # Iterate through the data and convert units
    for key, value in data.items():
        # Check to see if the value is a list and if so does it contain a unit?
        # Could also be a dictionary so need to go deeper.
        if value is list:
            if value[1] in compatible_units:
                # Convert units and replace value with just the converted number
                data[key] = value[0] * conversions[value[1]]
            else:
                raise ValueError("Units are not valid. Use: 'in', 'ft', 'cm', 'm', 'lb', 'kg'")
        else:
            if key == 'weight':
                data[key] = value * global_conversions['weight']
            else:
                data[key] = value * global_conversions['length']

    return data