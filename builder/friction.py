import xml.etree.ElementTree as ET
from typing import Protocol, List

class Friction:
    """ Assumes surface friction and the sovler to be ODE"""

    def __init__(self, mu: float, fdir1: List[float], mu2: float = None):
        print("Warning: Using friction that is not fully implemented to support all of Gazebo's functionality")
        self.mu = mu
        self.fdir1 = fdir1
        self.mu2 = mu2

    def create_tree(self) -> ET.Element:
        """ Example Output: 
            <surface>
                <friction>
                    <ode>
                        <mu>0.5</mu>
                        <mu2>1.0</mu2>
                        <fdir1>0 0 1</fdir1>
                    </ode>
                </friction>
            </surface>
        """
        surface = ET.Element('surface')
        friction = ET.SubElement(surface, 'friction')
        ode = ET.SubElement(friction, 'ode')

        mu = ET.Element('mu')
        mu.text = '{:0.3f}'.format(self.mu)
        ode.append(mu)

        if self.mu2 is not None:
            mu2 = ET.Element('mu2')
            mu2.text = '{:0.3f}'.format(self.mu2)
            ode.append(mu)

        fdir1 = ET.Element('fdir1')
        fdir1.text = '{:0.2f} {:0.2f} {:0.2f}'.format(*self.fdir1)
        ode.append(fdir1)

        return surface
