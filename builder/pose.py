import xml.etree.ElementTree as ET
from typing import List
from math import pi

class Pose():
    
    def __init__(self, translation: List[float], rotation: List[float], relative_to: str = None):
        """
            Tranlsation units are in meters.
            rotation units are in degrees and represent Roll, Pitch, Yaw 
            intrinsic(sequential) rotations.
            Rotation angle is stored as radians
            relative_to: stores the name of the link/frame the pose is relative to
        """
        self.translation = translation
        self.rotation = [(rot*pi / 180.0) for rot in rotation]
        self.relative_to = relative_to

    def create_tree(self) -> ET.Element:
        if self.relative_to is not None:
            pose = ET.Element('pose', attrib={'relative_to': self.relative_to})
        else:
            pose = ET.Element('pose')
        n = self.translation + self.rotation
        pose.text = '{:0.2f} {:0.2f} {:0.2f} {:0.4f} {:0.4f} {:0.4f}'.format(*n)
        return pose