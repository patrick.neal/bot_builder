import xml.etree.ElementTree as ET
from typing import List
from builder.geometries import Geometry, Inertial
from builder.joints import Joint
from builder.pose import Pose
from builder.ign_plugin import Plugin
from builder.friction import Friction

class Visual():
    """ 
        Visual can contain a basic geometry. Meshes are not currently supported.
        Add them manually after SDF generation. 
    """
    def __init__(self, name: str, geometry: Geometry, pose: Pose = None):
        self.name = name
        self.geom = geometry
        self.pose = pose

    def create_tree(self) -> ET.Element:
        """"""
        visual = ET.Element('visual', attrib={'name': self.name})
    
        if self.pose is not None:
            pose = self.pose.create_tree()
            visual.append(pose)

        geom = self.geom.create_tree()
        visual.append(geom)
        return visual


class Collision():

    def __init__(self, name: str, geometry: Geometry, friction: Friction = None, pose: Pose = None):
        self.name = name
        self.geom = geometry
        self.pose = pose
        self.friction = friction
    
    def create_tree(self) -> ET.Element:
        """"""
        collision = ET.Element('collision', attrib={'name': self.name})
    
        if self.pose is not None:
            pose = self.pose.create_tree()
            collision.append(pose)

        geom = self.geom.create_tree()
        collision.append(geom)

        if self.friction is not None:
            friction = self.friction.create_tree()
            collision.append(friction)
        
        return collision


class Link():
    """ 
        Links contain Collision Geometry, Visual Geometry and Inertia. 
        Must have at least a visual, use a Frame if all values are to be 'None".
        Pass "None" to values that you do not wish to specify.
    """
    def __init__(self, name: str, inertial: Inertial = None, collision: Collision = None, visuals: List[Visual] = None, pose: Pose = None)-> None:
        self.name = name
        self.inertial = inertial
        self.collision = collision
        self.visuals = visuals
        self.pose = pose

    def create_tree(self) -> ET.Element:
        link = ET.Element('link', attrib={'name': self.name})
        if self.pose is not None:
            pose = self.pose.create_tree()
            link.append(pose)
        if self.inertial is not None:
            inertial = self.inertial.create_tree()
            link.append(inertial)
        if self.visuals is not None:
            for visual in self.visuals:
                visual_el = visual.create_tree()
                link.append(visual_el)
        if self.collision is not None:
            collision = self.collision.create_tree()
            link.append(collision)

        return link


class Frame():

    def __init__(self, name: str, attached: str, pose: Pose):
        self.name = name
        self.attached = attached
        self.pose = pose
    
    def create_tree(self):
        frame = ET.Element('frame', {'name': self.name, 'attached_to': self.attached})
        pose = self.pose.create_tree()
        frame.append(pose)
        return frame


class Model():
    """ Model contains Links (required), Joints, and Frames. Sensors are not currently supported."""
    def __init__(self, name: str, canonical_link: str, links: List[Link], joints: List[Joint] = None, frames: List[Frame] = None, plugins: List[Plugin] = None):
        self.name = name
        self.links = links
        self.frames = frames
        self.canonical_link = canonical_link
        self.joints = joints
        self.plugins = plugins

    def create_tree(self):
        model = ET.Element('model', {'name': self.name, 'canonical_link': self.canonical_link})

        if self.frames is not None:
            for frame in self.frames:
                temp_element = frame.create_tree()
                model.append(temp_element)
        
        for link in self.links:
            temp_element = link.create_tree()
            model.append(temp_element)

        if self.joints is not None:
            for joint in self.joints:
                temp_element = joint.create_tree()
                model.append(temp_element)
        
        if self.plugins is not None:
            for plugin in self.plugins:
                temp_element = plugin.create_tree()
                model.append(temp_element)

        return model