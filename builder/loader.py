import importlib
from typing import List

class PluginInterface:
    
    @staticmethod
    def initialize() -> None:
        pass


def import_module(name: str) -> PluginInterface:
    return importlib.import_module(name)


def load_plugins(plugins: List[str]) -> None:
    for plugin_name in plugins:
        plugin = import_module(plugin_name)
        plugin.initialize()