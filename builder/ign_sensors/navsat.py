"""
 Requires the addition of spherical coordinates outside the link where the sensor is attached

"""
import xml.etree.ElementTree as ET

"""
    * Must be at the "plugin" section of the .sdf (the top under the world statement)
        can this be placed in the model.sdf or does it have to be in world sdf?
    <plugin
        filename="ignition-gazebo-navsat-system"
        name="ignition::gazebo::systems::NavSat">
    </plugin>

    <spherical_coordinates>
        <surface_model>EARTH_WGS84</surface_model>
        <world_frame_orientation>ENU</world_frame_orientation>
        <latitude_deg>-22.986687</latitude_deg>
        <longitude_deg>-43.202501</longitude_deg>
        <elevation>0</elevation>
        <heading_deg>0</heading_deg>
    </spherical_coordinates>

    <sensor name="navsat" type="navsat">
        <always_on>1</always_on>
        <update_rate>1</update_rate>
        <topic>navsat</topic>
    </sensor>
"""

class NavSat:

    def create_tree(self) -> ET.Element:
        pass
