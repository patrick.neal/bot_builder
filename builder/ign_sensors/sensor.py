import xml.etree.ElementTree as ET
from typing import Protocol, List
from builder.pose import Pose

class Sensor(Protocol):

    def create_tree(self) -> ET.Element:
        """ create the XML elements"""
