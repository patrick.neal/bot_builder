from typing import Protocol, List

from builder.entities import Model

class RobotBuilder(Protocol):

    def build(self) -> List[Model]:
        """Collects the robot parameters from the user and assembles the model"""