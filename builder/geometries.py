import xml.etree.ElementTree as ET
from typing import Protocol, Dict, List
from math import pi

""" All entities should generate their respective XML tree corresponding to the SDF format"""

class Geometry(Protocol):

    def create_tree(self) -> ET.Element:
        """ Generate a ET.Element containing the geometry data"""

    def calculate_intertia(self, mass: float) -> Dict[str, float]:
        """"""


class Cylinder():

    def __init__(self, radius, width):
        self.radius = radius
        self.width = width
        
    def create_tree(self) -> ET.Element:
        geom = ET.Element('geometry')
        cylinder = ET.SubElement(geom, 'cylinder')
        radius = ET.SubElement(cylinder, 'radius')
        radius.text = '{:0.4f}'.format(self.radius)
        length = ET.SubElement(cylinder, 'length')
        length.text = '{:0.4f}'.format(self.width)
        return geom
    
    def calculate_inertia(self, mass: float) -> Dict[str, float]:
        inertia = {}
        inertia["ixx"] = 1 / 12 * mass * (3 * pow(self.radius, 2) + pow(self.width, 2))
        inertia["ixy"] = 0
        inertia["ixz"] = 0
        inertia["iyy"] = inertia["ixx"]
        inertia["iyz"] = 0
        inertia["izz"] = 1 / 2 * mass * pow(self.radius, 2)
        return inertia


class Rectangle():

    def __init__(self, length, width, height):
        self.length = length # "x, meters"
        self.width = width # "y, meters"
        self.height = height # "z, meters"
    
    def create_tree(self) -> ET.Element:
        geom = ET.Element('geometry')
        cylinder = ET.SubElement(geom, 'box')
        size = ET.SubElement(cylinder, 'size')
        size.text = '{:0.4f} {:0.4f} {:0.4f}'.format(self.length, self.width, self.height)
        return geom
    
    def calculate_inertia(self, mass: float) -> Dict[str, float]:
        inertia = {}
        inertia["ixx"] = 1 / 12 * mass * (pow(self.width, 2) + pow(self.height, 2))
        inertia["ixy"] = 0
        inertia["ixz"] = 0
        inertia["iyy"] = 1 / 12 * mass * (pow(self.height, 2) + pow(self.length, 2))
        inertia["iyz"] = 0
        inertia["izz"] = 1 / 12 * mass * (pow(self.width, 2) + pow(self.length, 2))
        return inertia


class Inertial():

    def __init__(self, geometry: Geometry, mass: float)-> None:
        self.mass = mass
        self.inertia = geometry.calculate_inertia(mass)

    def create_tree(self) -> ET.Element:
        """ create the inertial xml element """
        inertial = ET.Element('inertial')
        mass_element = ET.SubElement(inertial, 'mass')
        mass_element.text = '{:0.4f}'.format(self.mass)
        inertia_element = ET.SubElement(inertial, 'inertia')
        ixx = ET.SubElement(inertia_element, 'ixx')
        ixx.text = '{:0.4f}'.format(self.inertia['ixx'])
        ixy = ET.SubElement(inertia_element, 'ixy')
        ixy.text = '{:0.4f}'.format(self.inertia['ixy'])
        ixz = ET.SubElement(inertia_element, 'ixz')
        ixz.text = '{:0.4f}'.format(self.inertia['ixz'])
        iyy = ET.SubElement(inertia_element, 'iyy')
        iyy.text = '{:0.4f}'.format(self.inertia['iyy'])
        iyz = ET.SubElement(inertia_element, 'iyz')
        iyz.text = '{:0.4f}'.format(self.inertia['iyz'])
        izz = ET.SubElement(inertia_element, 'izz')
        izz.text = '{:0.4f}'.format(self.inertia['izz'])
        return inertial
