import xml.etree.ElementTree as ET
from xml.dom import minidom
import os

from builder.entities import Model

def write_model(robot_model: Model):

    # Get the directory of this python project
    dir_path = os.path.dirname(os.path.realpath(__file__))
    dir_path = os.path.dirname(dir_path)

    path = os.path.join(dir_path, 'robots', robot_model.name)

    try:
        os.mkdir(path)
    except FileExistsError as e:
        print("Robot already exists. Overwriting existing robot")

    create_model_config(filepath=path, robot_name=robot_model.name)

    xml_elements = robot_model.create_tree()
    sdf = ET.Element('sdf', attrib={'version': '1.9'})
    sdf.append(xml_elements)
    xml_doc = ET.ElementTree(sdf)
    write_sdf(filename=path + '/model.sdf', xml_doc=xml_doc)


def create_model_config(filepath: str, robot_name: str):
    """
        <model>
            <name>robot_name</name>
            <sdf version="1.9">model.sdf</sdf>
        </model>
    """
    filename = filepath + '/model.config'
    model = ET.Element('model')
    name = ET.SubElement(model, 'name')
    name.text = robot_name
    sdf = ET.SubElement(model, 'sdf', attrib={'version': '1.9'})
    sdf.text = 'model.sdf'
    tree = ET.ElementTree(model)
    write_sdf(filename=filename, xml_doc=tree)


def write_sdf(filename: str, xml_doc: ET.ElementTree) -> None:
    xmlstr = minidom.parseString(ET.tostring(xml_doc.getroot())).toprettyxml(indent="    ")
    with open(filename, 'w') as file:
        file.write(xmlstr)
