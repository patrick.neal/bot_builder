from typing import Callable, Dict, Any
from builder.robot_builder import RobotBuilder

robot_creation_funcs: Dict[str, Callable[..., RobotBuilder]] = {}

def register(vehicle_type: str, creation_func: Callable[..., RobotBuilder]) -> None:
    robot_creation_funcs[vehicle_type] = creation_func


def unregister(vehicle_type: str) -> None:
    robot_creation_funcs.pop(vehicle_type, None)


def create(robot_type: str) -> RobotBuilder:
    try:
        creation_func = robot_creation_funcs[robot_type]
    except KeyError:
        raise ValueError(f"Unknown Robot Type {robot_type!r}")

    return creation_func()
