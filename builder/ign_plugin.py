import xml.etree.ElementTree as ET
from typing import Protocol, Dict, List

class Plugin(Protocol):
    def __init__(self):
        """"""

    def create_tree(self) -> ET.Element:
        """"""

class IgnAckermannPlugin:
    """
        Plugin XML definition is shown below.
        <plugin
            filename="ignition-gazebo-ackermann-steering-system"
            name="ignition::gazebo::systems::AckermannSteering">
            <left_joint>fl_wheel_joint</left_joint>
            <left_joint>rl_wheel_joint</left_joint>
            <right_joint>fr_wheel_joint</right_joint>
            <right_joint>rr_wheel_joint</right_joint>
            <left_steering_joint>fl_steer_joint</left_steering_joint>
            <right_steering_joint>fr_steer_joint</right_steering_joint>
            <kingpin_width>1.28</kingpin_width>
            <steering_limit>0.5</steering_limit>
            <wheel_base>1.86</wheel_base>
            <wheel_separation>1.68</wheel_separation>
            <wheel_radius>0.65532</wheel_radius>
            <min_velocity>-1</min_velocity>
            <max_velocity>4</max_velocity>
            <min_acceleration>-3</min_acceleration>
            <max_acceleration>3</max_acceleration>
        </plugin>
    """

    def __init__(self, params: Dict) -> None:
        self.filename = "ignition-gazebo-ackermann-steering-system"
        self.plugin_name = "ignition::gazebo::systems::AckermannSteering"
        self.params = params

    def create_tree(self) -> ET.Element:
        plugin = ET.Element('plugin', {'filename': self.filename, 'name': self.plugin_name})
        # Joint names required for the plugin
        if 'front_left_joint' in self.params:
            front_left_joint = ET.SubElement(plugin, 'left_joint')
            front_left_joint.text = self.params['front_left_joint']
        else:
            raise KeyError()
        if 'rear_left_joint' in self.params:
            rear_left_joint = ET.SubElement(plugin, 'left_joint')
            rear_left_joint.text = self.params['rear_left_joint']
        else:
            raise KeyError()
        if 'front_right_joint' in self.params:
            front_right_joint = ET.SubElement(plugin, 'right_joint')
            front_right_joint.text = self.params['front_right_joint']
        else:
            raise KeyError()
        if 'rear_right_joint' in self.params:
            rear_right_joint = ET.SubElement(plugin, 'right_joint')
            rear_right_joint.text = self.params['rear_right_joint']
        else:
            raise KeyError()
        if 'left_steering_joint' in self.params:
            left_steering_joint = ET.SubElement(plugin, 'left_steering_joint')
            left_steering_joint.text = self.params['left_steering_joint']
        else:
            raise KeyError()
        if 'right_steering_joint' in self.params:
            right_steering_joint = ET.SubElement(plugin, 'right_steering_joint')
            right_steering_joint.text = self.params['right_steering_joint']
        else:
            raise KeyError()
        # Vehicle parameters used in the plugin
        if 'kingpin_width' in self.params:
            kingpin_width = ET.SubElement(plugin, 'kingpin_width')
            kingpin_width.text = '{:0.4f}'.format(self.params['kingpin_width'])
        else:
            raise KeyError()
        if 'wheelbase' in self.params:
            wheelbase = ET.SubElement(plugin, 'wheelbase')
            wheelbase.text = '{:0.4f}'.format(self.params['wheelbase'])
        else:
            raise KeyError()
        if 'wheel_seperation' in self.params:
            wheel_seperation = ET.SubElement(plugin, 'wheel_seperation')
            wheel_seperation.text = '{:0.4f}'.format(self.params['wheel_seperation'])
        else:
            raise KeyError()
        if 'wheel_radius' in self.params:
            wheel_radius = ET.SubElement(plugin, 'wheel_radius')
            wheel_radius.text = '{:0.4f}'.format(self.params['wheel_radius'])
        else:
            raise KeyError()
        # Optional Inputs based on your need
        if 'steering_limit' in self.params:
            steering_limit = ET.SubElement(plugin, 'steering_limit')
            steering_limit.text = '{:0.4f}'.format(self.params['steering_limit'])
        if 'min_velocity' in self.params:
            min_velocity = ET.SubElement(plugin, 'min_velocity')
            min_velocity.text = '{:0.4f}'.format(self.params['min_velocity'])
        if 'max_velocity' in self.params:
            min_velocity = ET.SubElement(plugin, 'max_velocity')
            min_velocity.text = '{:0.4f}'.format(self.params['max_velocity'])
        if 'min_acceleration' in self.params:
            min_velocity = ET.SubElement(plugin, 'min_acceleration')
            min_velocity.text = '{:0.4f}'.format(self.params['min_acceleration'])
        if 'max_acceleration' in self.params:
            min_velocity = ET.SubElement(plugin, 'max_acceleration')
            min_velocity.text = '{:0.4f}'.format(self.params['max_acceleration'])

        return plugin

class CimarAckermannPlugin:
    """
        Plugin XML definition is shown below.
        <plugin filename="AckermannSteering" name="cimar::AckermannSteering">
            <topic>/kubota/cmd_vel</topic>
            <left_joint>fl_wheel_joint</left_joint>
            <left_joint>rl_wheel_joint</left_joint>
            <right_joint>fr_wheel_joint</right_joint>
            <right_joint>rr_wheel_joint</right_joint>
            <left_steering_joint>fl_steer_joint</left_steering_joint>
            <right_steering_joint>fr_steer_joint</right_steering_joint>
            <kingpin_width>1.3411</kingpin_width>
            <wheelbase>2.1107</wheelbase>
            <track>1.5189</track>
            <front_wheel_radius>0.5283</front_wheel_radius>
            <rear_wheel_radius>0.7429</rear_wheel_radius>
            <steering_limit>0.5236</steering_limit>
            <min_velocity>-5.0000</min_velocity>
            <max_velocity>5.0000</max_velocity>
            <min_acceleration>-3.0000</min_acceleration>
            <max_acceleration>2.0000</max_acceleration>
        </plugin>
    """

    def __init__(self, params: Dict) -> None:
        self.filename = "AckermannSteering"
        self.plugin_name = "cimar::AckermannSteering"
        self.params = params

    def create_tree(self) -> ET.Element:
        plugin = ET.Element('plugin', {'filename': self.filename, 'name': self.plugin_name})
        # Joint names required for the plugin
        if 'front_left_joint' in self.params:
            front_left_joint = ET.SubElement(plugin, 'left_joint')
            front_left_joint.text = self.params['front_left_joint']
        else:
            raise KeyError()
        if 'rear_left_joint' in self.params:
            rear_left_joint = ET.SubElement(plugin, 'left_joint')
            rear_left_joint.text = self.params['rear_left_joint']
        else:
            raise KeyError()
        if 'front_right_joint' in self.params:
            front_right_joint = ET.SubElement(plugin, 'right_joint')
            front_right_joint.text = self.params['front_right_joint']
        else:
            raise KeyError()
        if 'rear_right_joint' in self.params:
            rear_right_joint = ET.SubElement(plugin, 'right_joint')
            rear_right_joint.text = self.params['rear_right_joint']
        else:
            raise KeyError()
        if 'left_steering_joint' in self.params:
            left_steering_joint = ET.SubElement(plugin, 'left_steering_joint')
            left_steering_joint.text = self.params['left_steering_joint']
        else:
            raise KeyError()
        if 'right_steering_joint' in self.params:
            right_steering_joint = ET.SubElement(plugin, 'right_steering_joint')
            right_steering_joint.text = self.params['right_steering_joint']
        else:
            raise KeyError()
        # Vehicle parameters used in the plugin
        if 'kingpin_width' in self.params:
            kingpin_width = ET.SubElement(plugin, 'kingpin_width')
            kingpin_width.text = '{:0.4f}'.format(self.params['kingpin_width'])
        else:
            raise KeyError()
        if 'wheelbase' in self.params:
            wheelbase = ET.SubElement(plugin, 'wheelbase')
            wheelbase.text = '{:0.4f}'.format(self.params['wheelbase'])
        else:
            raise KeyError()
        if 'track' in self.params:
            wheel_seperation = ET.SubElement(plugin, 'track')
            wheel_seperation.text = '{:0.4f}'.format(self.params['track'])
        else:
            raise KeyError()
        if 'front_wheel_radius' in self.params:
            wheel_radius = ET.SubElement(plugin, 'front_wheel_radius')
            wheel_radius.text = '{:0.4f}'.format(self.params['front_wheel_radius'])
        else:
            raise KeyError()
        if 'rear_wheel_radius' in self.params:
            wheel_radius = ET.SubElement(plugin, 'rear_wheel_radius')
            wheel_radius.text = '{:0.4f}'.format(self.params['rear_wheel_radius'])
        else:
            raise KeyError()
        # Optional Inputs based on your need
        if 'steering_limit' in self.params:
            steering_limit = ET.SubElement(plugin, 'steering_limit')
            steering_limit.text = '{:0.4f}'.format(self.params['steering_limit'])
        if 'min_velocity' in self.params:
            min_velocity = ET.SubElement(plugin, 'min_velocity')
            min_velocity.text = '{:0.4f}'.format(self.params['min_velocity'])
        if 'max_velocity' in self.params:
            min_velocity = ET.SubElement(plugin, 'max_velocity')
            min_velocity.text = '{:0.4f}'.format(self.params['max_velocity'])
        if 'min_acceleration' in self.params:
            min_velocity = ET.SubElement(plugin, 'min_acceleration')
            min_velocity.text = '{:0.4f}'.format(self.params['min_acceleration'])
        if 'max_acceleration' in self.params:
            min_velocity = ET.SubElement(plugin, 'max_acceleration')
            min_velocity.text = '{:0.4f}'.format(self.params['max_acceleration'])

        return plugin

