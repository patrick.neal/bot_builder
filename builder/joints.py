import xml.etree.ElementTree as ET
from typing import Protocol, List
from builder.pose import Pose

"""
    List of Joint Types
        continuous: A hinge joint that rotates on a single axis with a continous range of motion
        revolute(Supported): A hinge joint that rotates on a single axis with a fixed ranged of motion
        gearbox: A geared revolute joint
        revolute2(Supported): Two revolute joints connected in series (better way to explain?)
        prismatic: A sliding joint that slides along an axis with a limited
        ball: a ball and socket joint
        screw: a single degree of freedom joint with coupled sliding and rotational motion
        universal: Like a ball joint, but contrains one degree of freedom
        fixed(Supported): A joint with zero degrees of freeedom that rigidly connects two links
"""

class Joint(Protocol):

    def create_tree(self) -> ET.Element:
        """ create the XML elements"""

class Axis:

    def __init__(self, axis_vector: List[float], expressed_in: str = None, second_axis: bool = False):
        self.axis_direction = axis_vector
        self.axis_name = 'axis' if not second_axis else 'axis2'
        self.expressed_in = expressed_in

    def create_tree(self) -> ET.Element:
        axis = ET.Element(self.axis_name)
        if self.expressed_in is not None:
            xyz = ET.Element('xyz', attrib={'expressed_in': self.expressed_in})
        else:
            xyz = ET.Element('xyz')
        xyz.text = '{:0.2f} {:0.2f} {:0.2f}'.format(*self.axis_direction)
        axis.append(xyz)
        return axis


class Continuous:

    def __init__(self, name: str, parent: str, child: str, axis: Axis, pose: Pose = None):
        raise NotImplementedError("Gazebo does not seem to support Continuous joints")
        self.name = name
        self.parent = parent
        self.child = child
        self.axis = axis
        self.pose = pose
    
    def create_tree(self) -> ET.Element:
        joint = ET.Element('joint', attrib={'name': self.name, 'type': 'continuous'})
        parent = ET.SubElement(joint, 'parent')
        parent.text = self.parent
        child = ET.SubElement(joint, 'child')
        child.text = self.child
        axis = self.axis.create_tree()
        joint.append(axis)
        if self.pose is not None:
            pose = self.pose.create_tree()
            joint.append(pose)
        return joint


class Revolute:
    
    def __init__(self, name: str, parent: str, child: str, axis: Axis, pose: Pose = None):
        self.name = name
        self.parent = parent
        self.child = child
        self.axis = axis
        self.pose = pose

    def create_tree(self) -> ET.Element:
        joint = ET.Element('joint', attrib={'name': self.name, 'type': 'revolute'})
        parent = ET.SubElement(joint, 'parent')
        parent.text = self.parent
        child = ET.SubElement(joint, 'child')
        child.text = self.child
        axis = self.axis.create_tree()
        joint.append(axis)
        if self.pose is not None:
            pose = self.pose.create_tree()
            joint.append(pose)
        return joint


class Revolute2:
    
    def __init__(self, name: str, parent: str, child: str, axis: Axis, axis2: Axis, pose: Pose = None):
        self.name = name
        self.parent = parent
        self.child = child
        self.axis = axis
        self.axis2 = axis2
        self.pose = pose

    def create_tree(self) -> ET.Element:
        joint = ET.Element('joint', attrib={'name': self.name, 'type': 'revolute2'})
        parent = ET.SubElement(joint, 'parent')
        parent.text = self.parent
        child = ET.SubElement(joint, 'child')
        child.text = self.child
        axis = self.axis.create_tree()
        joint.append(axis)
        axis2 = self.axis2.create_tree()
        joint.append(axis2)
        if self.pose is not None:
            pose = self.pose.create_tree()
            joint.append(pose)
        return joint

class Fixed:
    
    def __init__(self, name: str, parent: str, child: str, pose: Pose = None):
        self.name = name
        self.parent = parent
        self.child = child
        self.pose = pose

    def create_tree(self) -> ET.Element:
        joint = ET.Element('joint', attrib={'name': self.name, 'type': 'fixed'})
        parent = ET.SubElement(joint, 'parent')
        parent.text = self.parent
        child = ET.SubElement(joint, 'child')
        child.text = self.child
        if self.pose is not None:
            pose = self.pose.create_tree()
            joint.append(pose)
        return joint
